CREATE DATABASE EMP;
USE EMP;

CREATE TABLE Employee(
	Empid int not null primary key identity(1,1),
	Empname nvarchar(150),
	Email nvarchar(150),
	Age int,
	Salary int
);