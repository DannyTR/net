﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Core_Clientes.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Core_Clientes.Controllers
{
    public class ClienteController : Controller
    {
        private readonly ApplicationDbContext _db;
        public ClienteController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            var displaydata = _db.Cliente.ToList();
            return View(displaydata);
        }

        [HttpGet] //Tiene un parametro
        public async Task<IActionResult> Index(string ctleSearch)
        {
            ViewData["GetClienteDetails"] = ctleSearch;
            var empquery = from x in _db.Cliente select x;
            if (!String.IsNullOrEmpty(ctleSearch))
            {
                empquery = empquery.Where(x => x.NombreCliente.Contains(ctleSearch) || x.Correo.Contains(ctleSearch));
            }
            return View(await empquery.AsNoTracking().ToListAsync());
        }

        public IActionResult Create()
        {
            return View();
        }

        //2)Crear cliente
        [HttpPost]
        public async Task<IActionResult> Create(Cliente nClte)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nClte);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(nClte);
        }

        //3) Detalles cliente
        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDetail = await _db.Cliente.FindAsync(id);
            return View(getEmpDetail);
        }

        //4)Editar cliente
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getCtleDetail = await _db.Cliente.FindAsync(id);
            return View(getCtleDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Cliente oldCtle)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldCtle);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldCtle);
        }

        //5)Eliminar cliente
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getCtleDetails = await _db.Cliente.FindAsync(id);
            return View(getCtleDetails);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getCtledetail = await _db.Cliente.FindAsync(id);
            _db.Cliente.Remove(getCtledetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
