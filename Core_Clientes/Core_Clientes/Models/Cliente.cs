﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core_Clientes.Models
{
    public class Cliente
    {
        [Key]
        public int IdCliente { get; set; }

        [Required(ErrorMessage = "Ingresa nombre de cliente")]
        [Display(Name = "Nombre Cliente")]
        public string NombreCliente { get; set; }

        [Required(ErrorMessage = "Ingresa correo del cliente")]
        [Display(Name = "Correo")]
        [EmailAddress(ErrorMessage = "Ingresa un correo válido")]
        public string Correo { get; set; }

        [Required(ErrorMessage = "Ingrese edad del cliente")]
        [Display(Name = "Edad")]
        [Range(20, 50)]
        public int EdadCliente { get; set; }

        [Required(ErrorMessage = "Ingresa crédito")]
        [Display(Name = "Crédito")]
        public int CreditoCliente { get; set; }
    }
}
